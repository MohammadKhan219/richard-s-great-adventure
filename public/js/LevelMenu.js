GreatAdventure.LevelMenu = function(){};

GreatAdventure.LevelMenu.prototype = {
    create: function() {
            this.level_menu_screen = this.add.sprite(0, 0, 'grey_bg');

            this.tower1 = this.add.sprite(50, 80, 'tower1');
            this.tower2 = this.add.sprite(320, 80, 'tower2');
            this.tower3 = this.add.sprite(590, 80, 'tower3');

            // cp1 = hardFeet, cp2 = speedBonus, cp3 = fireDelayReduction, 
            // cp4 = this.player.life, cp5 = this.player.coins

            this.tower1.inputEnabled = true;
            this.tower1.events.onInputDown.add(function() {
                this.state.start('Game', true, false, false,0,0,5,0);
            }, this);

            this.tower2.inputEnabled = true
            this.tower2.events.onInputDown.add(function() {
                this.state.start('Level2', true, false, false,0,0,5,0);
            }, this);

            this.tower3.inputEnabled = true
            this.tower3.events.onInputDown.add(function() {
                this.state.start('Level3', true, false, false,0,0,5,0);
            }, this);

            this.back_button = this.add.sprite(this.game.world.centerX-125, 480, 'back_button');
            this.back_button.inputEnabled = true;
            this.back_button.events.onInputDown.add(function() {this.state.start('MainMenu')}, this);
        },

}