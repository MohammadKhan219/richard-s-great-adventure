GreatAdventure.MainMenu = function(){};

GreatAdventure.MainMenu.prototype = {
    create: function() {
        this.theme = this.game.add.audio("main_theme");
        this.theme.play('', 0, 1,true, false);
        this.play_button = this.add.sprite(this.game.world.centerX-125, this.game.world.centerY-140, 'play_button');
        this.play_button.inputEnabled = true;
        this.play_button.events.onInputDown.add(function() {this.theme.stop();this.state.start('Game');}, this);

        this.level_select_button = this.add.sprite(this.game.world.centerX-125, this.game.world.centerY-80, 'level_select_button');
        this.level_select_button.inputEnabled = true;
        this.level_select_button.events.onInputDown.add(function() {this.theme.stop();this.state.start('LevelMenu');}, this);

        this.controls_button = this.add.sprite(this.game.world.centerX-125, this.game.world.centerY-20, 'controls_button');
        this.controls_button.inputEnabled = true;
        this.controls_button.events.onInputDown.add(function() {this.theme.stop();this.state.start('ControlMenu');}, this);
        
        this.help_button = this.add.sprite(this.game.world.centerX-125, this.game.world.centerY+40, 'help_button');
        this.help_button.inputEnabled = true;
        this.help_button.events.onInputDown.add(function() {this.theme.stop();this.game.state.start('HelpMenu');}, this);
    }

}