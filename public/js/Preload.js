var GreatAdventure = GreatAdventure || {};
GreatAdventure.Preload = function(){};

GreatAdventure.Preload.prototype = {
    preload: function() {
        this.splash = this.add.sprite(this.game.world.centerX, this.game.world.centeryY, 'logo');
        this.load.spritesheet("richard", "./images/game assets/richard.png",32, 32 );
        this.load.spritesheet("slime", "./images/game assets/Slime.png",32,32);
        this.load.image("slime_bullet", "./images/game assets/slime_proj.png");
        this.load.spritesheet("blue_slime", "./images/game assets/Blue_Slime.png",32,32);
        this.load.image("blue_slime_bullet", "./images/game assets/blue_slime_proj.png");
        this.load.spritesheet("red_slime", "./images/game assets/Red_Slime.png",32,32);
        this.load.image("red_slime_bullet", "./images/game assets/red_slime_proj.png");
        this.load.spritesheet("shopkeep", "./images/game assets/Shopkeeper.png", 48,64);
        this.load.image("heart", "./images/game assets/Heart.png");
        this.load.image("bullet", "./images/game assets/energy_blast.png");
        this.load.tilemap("tower1", "./images/game assets/tower1.json", null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap("tower2", "./images/game assets/tower2.json", null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap("tower3", "./images/game assets/tower3.json", null, Phaser.Tilemap.TILED_JSON);
        this.load.image("coin", "./images/game assets/coin.png");
        this.load.image("small_rock", "./images/game assets/Small Rock.png");
        this.load.image("big_rock", "./images/game assets/Big Rock.png");
        this.load.image("tower_tiles", "./images/game assets/380tilesheet.png");
        this.load.image("back_button", "./images/game assets/Back_Button.png");
        this.load.image("controls_button", "./images/game assets/Controls_Button.png");
        this.load.image("exit_button", "./images/game assets/Exit_Button.png");
        this.load.image("help_button", "./images/game assets/Help_Button.png");
        this.load.image("level_select_button", "./images/game assets/Level_Select_Button.png");
        this.load.image("help_button", "./images/game assets/Help_Button.png");
        this.load.image("main_menu_button", "./images/game assets/Main_Menu_Button.png");
        this.load.image("play_button", "./images/game assets/Play_Button.png");
        this.load.image("resume_button", "./images/game assets/Resume_Button.png");
        this.load.image("pause_menu", "./images/game assets/pause_menu.png");
        this.load.image("game_over_screen", "./images/game assets/game_over_screen.png");
        this.load.image("turret", "./images/game assets/Turret.png");
        this.load.image("ice_shot", "./images/game assets/Ice_Shot.png");
        this.load.image("boots", "./images/game assets/Boots.png");
        this.load.image("coffee", "./images/game assets/Coffee.png");
        this.load.image("popsicle", "./images/game assets/Popsicle.png");
        this.load.image("armor", "./images/game assets/Light Armor.png");
        this.load.image("boots", "./images/game assets/Boots.png");
        this.load.image("sword", "./images/game assets/Sword.png");
        this.load.image("door", "./images/game assets/Door.png");

        this.load.spritesheet("green_dragon", "./images/game assets/Green_Dragon.png",96,128);
        this.load.spritesheet("blue_dragon", "./images/game assets/Blue_Dragon.png",96,128);
        this.load.spritesheet("red_dragon", "./images/game assets/Red_Dragon.png",96,128);
        this.load.image("green_fireball", "./images/game assets/Green_Fireball.png");
        this.load.image("blue_fireball", "./images/game assets/Blue_Fireball.png");
        this.load.image("red_fireball", "./images/game assets/Red_Fireball.png");

        this.load.image("shop", "./images/game assets/shop-menu.png");
        this.load.image("grey_bg", "./images/game assets/grey_bg.png");
        this.load.image("tower1", "./images/game assets/tower1.png");
        this.load.image("tower2", "./images/game assets/tower2.png");
        this.load.image("tower3", "./images/game assets/tower3.png");
        // this.load.image("tower4", "./images/game assets/grey_bg.png");
        this.load.image("control_screen", "./images/game assets/control_screen.png");
        this.load.image("splash_screen", "./images/game assets/splash_screen.png");
        this.load.image("help_screen", "./images/game assets/help_screen.png");

        this.game.load.audio("coin_pickup", "./images/game assets/Coin.wav");
        this.game.load.audio("dying", "./images/game assets/Richard_Dying.wav");
        this.game.load.audio("jump", "./images/game assets/jump.wav");
        this.game.load.audio("slime_death", "./images/game assets/Slime_death.wav");
        this.game.load.audio("attack", "./images/game assets/Richard_Attack.wav");
        this.game.load.audio("bosstheme","./images/game assets/Boss_Battle.mp3");
        // add more audio here
        
    },

    create: function() {
        this.theme = this.game.add.audio("main_theme");
        this.theme.play('', 0, 1,true, false);
        this.splash_screen = this.add.sprite(0, 0, 'splash_screen');
        this.splash_screen.inputEnabled = true;
        this.splash_screen.events.onInputDown.add(function() {this.theme.stop();this.state.start('MainMenu');}, this);

    }
}