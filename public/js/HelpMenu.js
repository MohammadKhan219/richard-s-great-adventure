GreatAdventure.HelpMenu = function(){};

GreatAdventure.HelpMenu.prototype = {
    create: function() {
            this.help_menu_screen = this.add.sprite(0, 0, 'help_screen');

            this.back_button = this.add.sprite(this.game.world.centerX-125, 480, 'back_button');
            this.back_button.inputEnabled = true;
            this.back_button.events.onInputDown.add(function() {this.state.start('MainMenu')}, this);
        },

}