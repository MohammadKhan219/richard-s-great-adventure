GreatAdventure.Level3 = function(){};

GreatAdventure.Level3.prototype = {
    create: function() {
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.paused = false;
        this.setupAudio();
        this.setupMap("tower3", "380tilesheet");
        this.setupBulletPool();
        this.setupCoin();
        this.setupShopKeeper();
         this.setupBoss();
        this.setupPlayer();
        this.setupSlimes();

        this.setupKeys();
        this.setupUI();
        this.setupFireBalls();
        this.setupItems();
        this.setupDoor();
       
    },

    update: function() {
        
            this.collisions();
            if(this.player.life>0) {
                this.playerInput(); 
                this.slimeAI(); 
            }
                
            else 
                this.player.animations.play("dying");
            //this.updateHeight();
            this.spawnShopKeeper();
            this.bossAI();
            if((this.player.x <= this.shopKeeper.x -15 || this.player.x >=this.shopKeeper.x+15)&&this.shopKeeper.shopOpen) {
                this.shopMenu.destroy();
                this.shopKeeper.shopOpen = false;
            }
                   
    },

   

    setupMap: function(towerKey, tilesetKey) {
        //loading the tilemap
        this.map = this.game.add.tilemap(towerKey);
        this.map.addTilesetImage(tilesetKey, "tower_tiles");

        //make layers
        this.backgroundLayer = this.map.createLayer("Background");
        this.blockedLayer = this.map.createLayer("Collision Layer");
        this.lavaLayer = this.map.createLayer("Lava");
        //this.spikedLayer = this.map.createLayer("Spike");
        this.enemyBoundsLayer = this.map.createLayer("Enemy Bounds");
        this.enemyBoundsLayer.alpha = 0.0;

        this.map.setCollisionBetween(1,2000, true, "Collision Layer");
        this.map.setCollisionBetween(1,2000, true, "Lava");
        //this.map.setCollisionBetween(1,100, true, "Spike");
        this.map.setCollisionBetween(1,1000, true, "Enemy Bounds");
        
        this.backgroundLayer.resizeWorld();
    },

    setupPlayer: function() {
        var playerLocation = this.findObjectsByType("player_start", this.map, "Object");
        this.player = this.game.add.sprite(playerLocation[0].x, playerLocation[0].y, "richard");

        this.game.physics.arcade.enable(this.player);
        this.player.life = this.life;
        this.player.body.gravity.y = 300;
        

        this.player.animations.add("walking", [8,9,10,11],5,true);
        this.player.animations.add("jumping", [16,17,18,19], 5, true);
        this.player.animations.add("damage", [24],5, true);
        this.player.animations.add("idle", [0,1], 5, true);
        this.player.attackingAnimation = this.player.animations.add("attack", [4,5], 5, false, true);
        this.player.attackingAnimation.onComplete.add(this.turnOffAttacking, this);
        this.player.dyingAnimation = this.player.animations.add("dying", [24,25,26,27,28,29],3,false,true);
        this.player.dyingAnimation.onComplete.add(this.gameOver, this);

        this.player.invincible = false;
        this.player.body.collideWorldBounds = true;
        this.player.attacking = false;
        this.player.fireRate = 1000;
        this.player.nextFire = 0;
        this.player.anchor.setTo(.5,.5);

        this.player.speedBonus = this.speedBonus;
        this.player.fireDelayReduction = this.fire;

        this.player.hardFeet = this.hardFeet;
        

        this.player.invincibleCheat = false;
        this.player.collisionCheat = false;

        this.player.coins = this.playerCoin;

        this.game.camera.follow(this.player);
    },

    setupBoss: function() {
        var bossLocation = this.findObjectsByType("boss_start", this.map,"Object");
        this.boss = this.game.add.sprite(bossLocation[0].x-96, bossLocation[0].y-128, "green_dragon");
        this.game.physics.arcade.enable(this.boss);
        this.boss.enableBody = true;
        this.boss.life = 25;
        this.boss.body.gravity.y = 300;
        this.boss.anchor.setTo(.5,.5);
        this.boss.body.immovable = true;
        this.boss.moving = true;
        this.boss.breathing = false;
        this.boss.slamming = false;

        this.boss.nextFire = 0;
        this.boss.fireRate = 1000;

        this.boss.body.velocity.x = 200;
        this.boss.scale.setTo(-1,1);

        this.boss.animations.add("moving", [0,1,2,3,4], 5, true);
        this.bossBreathe = this.boss.animations.add("breathing", [8,9,10,11,12],5,false, true);
        this.bossSlam = this.boss.animations.add("slamming", [13,14,15,16,17,18,19],5, false, true)
        //this.bossBreathe.onComplete.add(this.bossAttack, this);
        //this.bossSlam.onComplete.add(this.bossSlamAttack, this);

        this.boss.breathTimer = this.game.time.create(false);
        this.boss.breathTimer.start();

        this.boss.breathTimer.add(Phaser.Timer.SECOND * this.game.rnd.realInRange(1, 3), this.bossAttack,this);
    },

    bossAI: function() {
         if(this.game.time.now > this.boss.nextFire && this.bossBulletPool.countDead() >=3) {
            
            this.boss.nextFire = this.game.time.now + this.boss.fireRate;
            this.boss.breathTimer.add(Phaser.Timer.SECOND * this.game.rnd.realInRange(1, 3), this.bossAttack,this);
        }
    },

    bossAttack: function() {
    
            this.boss.animations.play("breahting");
            for(var i = 0; i < 3; i++) {
                var bullet = this.bossBulletPool.getFirstDead();
                bullet.reset(this.boss.x, this.boss.y);
                bullet.scale.x = this.player.scale.x;
                this.game.physics.arcade.moveToXY(bullet,this.player.x + (Math.random()*(50-10+1)+10), this.player.y-this.player.height/2-(Math.random()*(10-5+1)+5), 60, 1000); 
            }
        
       
    },

    bossBulletHit: function(player, bullet) {
        bullet.kill();
        if(!this.player.invincible && !this.player.invincibleCheat) {
            this.player.life -=1;
            this.player.tint = 0xff0000;
            this.player.invincible = true;
            this.game.time.events.add(2000, this.toggleInvincible, this);
            this.writeLife();
        }
    },

    bossPlayerBulletHit: function(boss, bullet) {
        bullet.kill();
        this.boss.life -= 1;
        if(this.boss.life<=0) {
            boss.kill();
             this.nextDoor = this.create(900,352,'door');
        }
        
    },

    setupSlimes: function() {
        var slimeLocations = this.findObjectsByType("enemy_start", this.map, "Object");
        this.slimes = this.game.add.group();
        this.slimes.enableBody = true;

        for(var i = 0; i < slimeLocations.length; i++) {
            this.slime = this.slimes.create(slimeLocations[i].x, slimeLocations[i].y, "red_slime");

            this.slime.body.gravity.y = 300;
            this.slime.body.velocity.x = 300;
            this.slime.body.collideWorldBounds = true;
        
            this.slime.anchor.setTo(.5,.5);

            this.slime.moving = true;
            this.slime.damaging = false;
            this.slime.attacking = false;
            this.slime.jumping = true;
            this.slime.fireRate = 1000;
            this.slime.nextFire = 0;

            this.slime.animations.add("moving",[0,1,2,3], 5, true);
            this.slime.attackingAnimation = this.slime.animations.add("attack", [4,5,6,7],5, false, true);
            this.slime.attackingAnimation.onComplete.add(this.turnOffAttackingSlime, this);
            this.slime.damagingAnimation = this.slime.animations.add("damaging", [8,9,10,11], 30,false, true);
            this.slime.damagingAnimation.onComplete.add(this.turnOnDamaging, this);
            this.slime.attackingAnimation = this.slime.animations.add("attack", [4,5,6,7],30, false, true);
            this.slime.attackingAnimation.onComplete.add(this.turnOffAttacking, this);
            this.slime.animations.play("moving");
        }
    },


    setupDoor: function(){
        this.door = this.game.add.group();
        this.door.enableBody = true;
        this.door.physicsBodyType = Phaser.Physics.ARCADE;
    },

    setupKeys: function() {
        this.cursors = this.game.input.keyboard.createCursorKeys();
        this.pause = this.game.input.keyboard.addKey(Phaser.Keyboard.ESC);
        this.attack = this.game.input.keyboard.addKey(Phaser.Keyboard.Z);
        this.permInvincible = this.game.input.keyboard.addKey(Phaser.Keyboard.I);
        this.collisionTunnel = this.game.input.keyboard.addKey(Phaser.Keyboard.C);
        this.levelOne = this.game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        this.levelTwo = this.game.input.keyboard.addKey(Phaser.Keyboard.TWO);
        this.levelThree = this.game.input.keyboard.addKey(Phaser.Keyboard.THREE);
        
        this.attack.onDown.add(this.spriteAttack, this);
        this.pause.onDown.add(this.pauseGame, this);

        this.permInvincible.onDown.add(this.togglePermInvincible, this);
        this.collisionTunnel.onDown.add(this.turnOffCollision, this);
        this.levelOne.onDown.add(function() {
            this.theme.stop();
            // cp1 = hardFeet, cp2 = speedBonus, cp3 = fireDelayReduction, 
            // cp4 = this.player.life, cp5 = this.player.coins
            this.game.state.start("Game", true, false, this.player.hardFeet,this.player.speedBonus,this.player.fireDelayReduction,this.player.life,this.player.coins);
        }, this);
        this.levelTwo.onDown.add(function() {
            this.theme.stop();
            // cp1 = hardFeet, cp2 = speedBonus, cp3 = fireDelayReduction, 
            // cp4 = this.player.life, cp5 = this.player.coins
            this.game.state.start("Level2", true, false, this.player.hardFeet,this.player.speedBonus,this.player.fireDelayReduction,this.player.life,this.player.coins);
        }, this);
        this.levelThree.onDown.add(function() {
            this.theme.stop();
            // cp1 = hardFeet, cp2 = speedBonus, cp3 = fireDelayReduction, 
            // cp4 = this.player.life, cp5 = this.player.coins
            this.game.state.start("Level3", true, false, this.player.hardFeet,this.player.speedBonus,this.player.fireDelayReduction,this.player.life,this.player.coins);
        }, this);
        this.jump = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        this.buyOne = this.game.input.keyboard.addKey(Phaser.Keyboard.A);
        this.buyTwo = this.game.input.keyboard.addKey(Phaser.Keyboard.S);
        this.buyThree = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
        this.buyFour = this.game.input.keyboard.addKey(Phaser.Keyboard.F);
        this.buyFive = this.game.input.keyboard.addKey(Phaser.Keyboard.G);

        this.buyOne.onDown.add(this.buyItem, this);
        this.buyTwo.onDown.add(this.buyItem, this);
        this.buyThree.onDown.add(this.buyItem, this);
        this.buyFour.onDown.add(this.buyItem, this);
        this.buyFive.onDown.add(this.buyItem, this);
    },

    setupBulletPool: function() {
        this.bullets = this.game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(50, "bullet");
        this.bullets.setAll("checkWorldBounds", true);
        this.bullets.setAll("outOfBoundsKill", true);

        this.slimeBullets = this.game.add.group();
        this.slimeBullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.slimeBullets.createMultiple(100, "red_slime_bullet");
        this.slimeBullets.setAll("checkWorldBounds", true);
        this.slimeBullets.setAll("outOfBoundsKill", true);

         this.bossBulletPool = this.game.add.group();
        this.bossBulletPool.enableBody = true;
        this.bossBulletPool.physicsBodyType = Phaser.Physics.ARCADE;
        this.bossBulletPool.createMultiple(50,"green_fireball");
        this.bossBulletPool.setAll("checkWorldBounds", true);
        this.bossBulletPool.setAll("outOfBoundsKill", true);
        
    },

     setupDoor: function(){
        this.door = this.game.add.group();
        //this.door.physicsBodyType = Phaser.Physics.ARCADE;
    },


    setupCoin: function() {
        this.coins = this.game.add.group();
        this.coins.enableBody = true;
        this.coins.physicsBodyType = Phaser.Physics.ARCADE;
        this.coins.createMultiple(50, "coin");
        this.coins.setAll("checkWorldBounds", true);
        this.coins.setAll("outOfBoundsKill", true);
        
    
    },

    setupUI: function() {
        this.coinsText = this.game.add.text(32, 0, "Coins: ", {fontSize: "16px", fill: "#fff"});
        this.coinsText.fixedToCamera = true;
        this.hearts = this.game.add.group();
        for(var i = 0; i < this.player.life; i++) {
            var heart = this.hearts.create(32+ i*(16+5), 16, "heart");
            heart.fixedToCamera = true;
        }

        //this.playerHeight = this.game.add.text(32, 16*2, "Player Height: ", {fontSize: "16px", fill: "#fff"});
        //this.playerHeight.fixedToCamera = true;

        this.pause_menu = this.game.add.sprite(300,100, 'pause_menu');
        this.pause_menu.fixedToCamera = true;
        this.pause_menu.alpha = 0.0;       
        
        this.game_over_screen = this.game.add.sprite(0,0, 'game_over_screen');
        this.game_over_screen.fixedToCamera = true;
        this.game_over_screen.alpha = 0.0;
    },

    setupShopKeeper: function() {
        this.shopKeeper = this.game.add.sprite(this.game.width/2, this.game.height, "shopkeep");
        this.game.physics.arcade.enable(this.shopKeeper);
        this.shopKeeper.animations.add("idle", [0,1,2,3,4], 5, true);
        this.shopKeeper.animations.play("idle");
        this.shopKeeper.alpha = 0.0;
        this.shopKeeper.shopOpen = false;
        this.shopKeeper.anchor.setTo(.5,.5);
    },

    setupAudio: function() {
        this.theme = this.game.add.audio("main_theme");
        this.theme.play('', 0, 1,true, false);
        this.coinPickup = this.game.add.audio("coin_pickup");
        this.death = this.game.add.audio("dying");
        this.jumpSound = this.game.add.audio("jump");
        this.slimeDeath = this.game.add.audio("slime_death");
        this.richardAttack = this.game.add.audio("attack");
        this.bossbattle = this.game.add.audio("bosstheme");
    },

    setupItems: function() {
        this.items = this.game.add.group();
        this.items.enableBody = true;
        this.items.physicsBodyType = Phaser.Physics.ARCADE;
        
        this.boots = this.items.create(0,0,"boots");
        this.boots.id = 0;
        this.boots.alpha = 0;

        this.armor = this.items.create(0,0,"armor");
        this.armor.id = 1;
        this.armor.alpha = 0;

        this.sword = this.items.create(0,0,"sword");
        this.sword.id = 2;
        this.sword.alpha = 0;

        this.coffee = this.items.create(0,0,"coffee");
        this.coffee.id = 3;
        this.coffee.alpha = 0;

        this.popsicle = this.items.create(0,0,"popsicle");
        this.popsicle.id = 4;
        this.popsicle.alpha = 0;    
    },

    init: function(cp1, cp2, cp3, cp4, cp5) {
        //cp1 = hardFeet
        this.hardFeet = cp1;
        //cp2 = speedBonus
        this.speedBonus = cp2;
        //cp3 = fireDelayReduction
        this.fireDelayReduction = cp3;
        this.life = cp4;
        this.playerCoin = cp5;
        //cp4 = this.player.life
        //cp5 = this.player.coins
    },

    collisions: function() {
        this.playerPlatformCollision = this.game.physics.arcade.collide(this.player, this.blockedLayer);
        if(!this.player.collisionCheat) {
            this.slimePlayerCollision = this.game.physics.arcade.collide(this.player, this.slimes, this.slimeHit.bind(this));
            this.spikePlayerCollision = this.game.physics.arcade.overlap(this.player, this.lavaLayer, this.spikeDamage.bind(this));
        }
        this.playerCoinOverlap = this.game.physics.arcade.overlap(this.player, this.coins, this.updateScore.bind(this));
        this.playerSlimeBulletCollision = this.game.physics.arcade.collide(this.player, this.slimeBullets, this.slimeBulletHit.bind(this));

        this.enemyBoundsCollision = this.game.physics.arcade.collide(this.slimes, this.enemyBoundsLayer, this.turn.bind(this));
        this.enemyPlatformCollision = this.game.physics.arcade.collide(this.slimes, this.blockedLayer,this.jump_slime.bind(this));

        this.slimeBulletCollision = this.game.physics.arcade.collide(this.slimes, this.bullets, this.bulletSlimeCollision.bind(this));
        this.bulletBlockedCollision = this.game.physics.arcade.collide(this.bullets, this.blockedLayer, this.bulletPlatformCollision.bind(this));
        this.slimeBulletPlatformCollision = this.game.physics.arcade.collide(this.slimeBullets, this.blockedLayer, this.bulletPlatformCollision.bind(this));
        
        this.coinPlatformCollision = this.game.physics.arcade.collide(this.blockedLayer, this.coins);

        this.shopKeeperCollision = this.game.physics.arcade.collide(this.blockedLayer, this.shopKeeper);
        this.playerShopMenu = this.game.physics.arcade.overlap(this.player, this.shopKeeper, this.openShop.bind(this));

        this.itemCollision = this.game.physics.arcade.collide(this.items, this.blockedLayer);
        this.playerItemPickup = this.game.physics.arcade.overlap(this.player,this.items, this.pickupItem.bind(this));

         this.bossBoundsCollision = this.game.physics.arcade.collide(this.boss, this.enemyBoundsLayer, this.bossTurn.bind(this));
        this.bossPlatformCollision = this.game.physics.arcade.collide(this.boss, this.blockedLayer);
        
        this.bossPlayerBulletCollision = this.game.physics.arcade.collide(this.boss, this.bullets, this.bossPlayerBulletHit.bind(this));
        this.bossBulletPoolCollision = this.game.physics.arcade.collide(this.bossBulletPool, this.blockedLayer, this.bulletPlatformCollision.bind(this));
        this.bossBulletPlayerCollision = this.game.physics.arcade.collide(this.player, this.bossBulletPool, this.bossBulletHit.bind(this));
        this.doorPlayerCollision = this.game.physics.arcade.overlap(this.player,this.door, this.doorCollision.bind(this));
    },

    playerInput: function() {
        this.player.body.velocity.x = 0;

        if(this.cursors.left.isDown) {
            this.player.body.velocity.x = -150 - this.player.speedBonus;
            this.player.scale.setTo(1,1);
            if(!this.player.attacking)
                this.player.animations.play("walking");
        }

        else if(this.cursors.right.isDown) {
            this.player.body.velocity.x = 150 +this.player.speedBonus;
            this.player.scale.setTo(-1,1);
            if(!this.player.attacking)
                this.player.animations.play("walking");
        }

        else if(!this.player.attacking)
            this.player.animations.play("idle");

        if(this.jump.isDown && (this.playerPlatformCollision ||this.spikePlayerCollision)) {
            this.player.body.velocity.y = -350;
            this.player.animations.play("jumping");
            this.player.jumping = true;
            this.jumpSound.play();
        }

        if(this.attack.isDown)
            this.spriteAttack();
    },

    doorCollision: function(){
        this.game.state.start("Level2", true, false, this.player.hardFeet,this.player.speedBonus,this.player.fireDelayReduction,this.player.life,this.player.coins);
    },

     findObjectsByType: function(type, map, layer) {
        var result = new Array();
        map.objects[layer].forEach(function(element){
             if(element.properties.type === type) {
                element.y -= map.tileHeight;
                result.push(element);
            }
        });
        return result;
    },

    turnOffAttacking: function(sprite, animation) {
        sprite.attacking = false;
        
    },


    turnOffAttackingSlime: function(sprite, animation) {
        sprite.attacking = false;
         if(!sprite.damaging) {
            sprite.attacking = false;
            sprite.moving = true;
            sprite.jumping = true;
            sprite.animations.play("moving");
            this.turn(sprite);
        }
    },

    turnOnDamaging: function(sprite, animation) {
        sprite.damaging = true;
        var coin = this.coins.getFirstDead();
        coin.reset(sprite.x, sprite.y-sprite.height);
        coin.body.gravity.y = 300; 
        sprite.kill();
    },

    spriteAttack: function(sprite, animation) {
        this.player.animations.play("attack");
        this.player.attacking = true;
        this.fire();
    },

    fire: function() {
        if(this.game.time.now > this.player.nextFire && this.bullets.countDead() >0) {
            this.richardAttack.play();
            this.player.nextFire = this.game.time.now + this.player.fireRate-this.player.fireDelayReduction;
            var bullet = this.bullets.getFirstDead();
            bullet.reset(this.player.x, this.player.y - bullet.height/2);
            bullet.scale.x = this.player.scale.x *-1;
            bullet.body.velocity.x = 300 * this.player.scale.x * -1;

        }
    },

    slimeHit: function(sprite, obj2) {
        if(this.player.attacking)
            obj2.kill;
        else
            this.spikeDamage(sprite, obj2);
    },

    spikeDamage: function(sprite, obj2) {
        if(!this.player.invincible && !this.player.invincibleCheat) {
            this.player.life -=1;
            this.player.tint = 0xff0000;
            this.player.invincible = true;
            this.game.time.events.add(2000, this.toggleInvincible, this);
            this.writeLife();
        }
    },

    toggleInvincible: function() {
        this.player.invincible = !this.player.invincible;
        if(!this.player.invincible)
            this.player.tint = 0xffffff;
    },

    turn: function(sprite, group) {
        if(sprite.body.velocity.x === 0 && sprite.scale.x ===1 && sprite.moving && !sprite.attacking) {
            sprite.body.velocity.x = -200;
            sprite.scale.setTo(-1,1);
        }

        else if(sprite.body.velocity.x === 0 && sprite.scale.x === -1 && sprite.moving && !sprite.attacking) {
            sprite.body.velocity.x = 200;
            sprite.scale.setTo(1,1);
        }
    },

    bossTurn: function(sprite, group) {
        if(sprite.body.velocity.x === 0 && sprite.scale.x ===1 && sprite.moving && !sprite.slamming && !sprite.breathing) {
            sprite.body.velocity.x = 200;
            sprite.scale.setTo(-1,1);
        }

        else if(sprite.body.velocity.x === 0 && sprite.scale.x === -1 && sprite.moving && !sprite.slamming && !sprite.breathing) {
            sprite.body.velocity.x = -200;
            sprite.scale.setTo(1,1);
        }
    },

    slimeAI: function(){
        for(var i = 0; i < this.slimes.children.length; i ++){
            var slime = this.slimes.children[i];
            if(slime.y == this.player.y  && this.game.time.now > slime.nextFire && this.slimeBullets.countDead() > 0 && slime.alive && !slime.damaging && !slime.attacking) {
                slime.nextFire = this.game.time.now + slime.fireRate;
                slime.animations.play("attack");
                slime.body.velocity.x = 0;
                slime.body.velocity.y = 0;
                slime.attacking = true;
                slime.moving = false;
                slime.jumping = false;
                slime.nextFire = this.game.time.now + slime.fireRate;
                var bullet = this.slimeBullets.getFirstDead();
                bullet.reset(slime.x, slime.y - bullet.height/2);
                this.game.physics.arcade.moveToObject(bullet, this.player, 60*(this.player.y/1500),1000*(this.player.y/1500) );
            }
            
        }
    },

    pauseGame: function() {
        if(!this.game.paused) {
            this.pause_menu.alpha = 1.0;
            this.game.paused = !this.game.paused;
            
        }
        else if(this.game.paused){
             this.pause_menu.alpha = 0.0;
             this.game.paused = !this.game.paused;
         }
    },

    togglePermInvincible: function() {
        this.player.invincibleCheat = !this.player.invincibleCheat;
    },

    turnOffCollision: function() {
        this.player.collisionCheat = !this.player.collisionCheat;
    },

    jump_slime: function(slime) {
        if(slime.jumping && !slime.attacking)
            slime.body.velocity.y = -200;
        //slime.animations.play("moving");
    },

    bulletSlimeCollision: function(obj1, obj2) {
        if(!obj1.damaging){
            this.slimeDeath.play();
            obj1.damaging = true;
            obj1.body.velocity.x = 0;
            obj1.body.velocity.y = 0;
            obj1.body.immovable = true;
            obj1.moving = false;
            obj1.jumping = false;
            obj1.animations.play("damaging");
            
        }
        obj2.kill();
        
    },

    bulletPlatformCollision: function(obj1, obj2) {
        obj1.kill();
    },

    updateScore: function(obj1, obj2) {
        obj2.kill();
        this.player.coins += 1;
        this.coinsText.text = "Coins: "  + this.player.coins;
        this.coinPickup.play();

    },

    writeLife: function () {
        this.hearts.forEach(function(heart){heart.kill()});
        this.hearts = this.game.add.group();
        for(var i = 0; i < this.player.life; i++) {
            var heart = this.hearts.create(32+ i*(16+5), 16, "heart");
            heart.fixedToCamera = true;
        }
        
    },
    
    slimeBulletHit: function(player, bullet) {
        bullet.kill();
        if(!this.player.invincible && !this.player.invincibleCheat) {
            this.player.life -=1;
            this.player.tint = 0xff0000;
            this.player.invincible = true;
            this.game.time.events.add(2000, this.toggleInvincible, this);
            this.writeLife();
        }
    },
    gameOver: function() {
        // TODO: ADD PLAYER DEATH + GAME OVER SCREEN
        if(this.player.life <= 0) {
            this.game_over_screen.alpha = 1.0;
            
        }
    },

    updateHeight: function() {
        this.playerHeight.text = "Player Height: " + this.player.y;
    },

    spawnShopKeeper: function() {

        this.shopKeeper.body.gravity.y = 300;
        this.shopKeeper.alpha = 1;
        
    },

    openShop: function() {
        if(!this.shopKeeper.shopOpen) {
            this.shopKeeper.shopOpen = true;
            this.shopMenu = this.game.add.sprite(this.shopKeeper.x-160/2, this.shopKeeper.y-58/2-this.shopKeeper.height/2, "shop");

        }
    },

     buyItem: function(itemNo) {
        if(itemNo.event.key === "a" && this.boots.alive && this.player.coins >=3 &&this.shopKeeper.shopOpen &&this.boots.alpha === 0) {
            this.player.coins-=3;
            this.updateScore();
            this.boots.x =this.shopKeeper.x+this.boots.width;
            this.boots.y =this.shopKeeper.y - this.boots.height-this.shopKeeper.height;
            this.boots.body.gravity.y = 300;
            this.boots.alpha = 1;
        }
        else if(itemNo.event.key === "s" && this.armor.alive && this.player.coins >=4&&this.shopKeeper.shopOpen&&this.armor.alpha === 0) {
            this.player.coins-=4;
            this.updateScore();
            this.armor.x =this.shopKeeper.x+this.armor.width;
            this.armor.y =this.shopKeeper.y - this.armor.height-this.shopKeeper.height;
            this.armor.body.gravity.y = 300;
            this.armor.alpha = 1;
            
        }
        else if(itemNo.event.key === "d" && this.sword.alive && this.player.coins>=5&&this.shopKeeper.shopOpen&&this.sword.alpha === 0) {
           this.player.coins-=5;
            this.updateScore();
            this.sword.x =this.shopKeeper.x+this.sword.width;
            this.sword.y =this.shopKeeper.y - this.sword.height-this.shopKeeper.height;
            this.sword.body.gravity.y = 300;
            this.sword.alpha = 1;
        }
        else if(itemNo.event.key === "f" && this.coffee.alive && this.player.coins>=1&&this.shopKeeper.shopOpen&&this.coffee.alpha === 0) {
            this.player.coins-=1;
            this.updateScore();
            this.coffee.x =this.shopKeeper.x+this.coffee.width;
            this.coffee.y =this.shopKeeper.y - this.coffee.height-this.shopKeeper.height;
            this.coffee.body.gravity.y = 300;
            this.coffee.alpha = 1;
        }
        else if(itemNo.event.key === "g"&&this.popsicle.alive && this.player.coins >=2&&this.shopKeeper.shopOpen&&this.popsicle.alpha === 0) {
            this.player.coins-=2;
            this.updateScore();
            this.popsicle.x =this.shopKeeper.x+this.popsicle.width;
            this.popsicle.y =this.shopKeeper.y - this.popsicle.height-this.shopKeeper.height;
            this.popsicle.body.gravity.y = 300;
            this.popsicle.alpha = 1;
        }
    },

    pickupItem: function(obj1, obj2) {
        if(obj2.id === 0) {
            obj2.kill();
            this.player.hardFeet = true;
        }
        else if(obj2.id === 1){
            obj2.kill();
            this.player.speedBonus += 100;
        }
        else if(obj2.id === 2) {
            obj2.kill();
            this.player.fireDelayReduction += 300;
        }
        else if(obj2.id === 3) {
            obj2.kill();
            this.player.life += 1;
            this.writeLife();
        }
        else if(obj2.id === 4) {
            obj2.kill();
            this.player.life +=3;
            this.writeLife();
        }
    }
}