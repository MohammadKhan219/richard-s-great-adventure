var GreatAdventure = GreatAdventure || {};

GreatAdventure.game = new Phaser.Game(800,600, Phaser.AUTO);

GreatAdventure.game.state.add('Boot', GreatAdventure.Boot); //load images for splash screen and menus
GreatAdventure.game.state.add('Preload', GreatAdventure.Preload); //load assets for everything else
GreatAdventure.game.state.add('MainMenu', GreatAdventure.MainMenu); //the main menu, all images are loaded
GreatAdventure.game.state.add('ControlMenu', GreatAdventure.ControlMenu);
GreatAdventure.game.state.add('HelpMenu', GreatAdventure.HelpMenu);
GreatAdventure.game.state.add('LevelMenu', GreatAdventure.LevelMenu);
GreatAdventure.game.state.add('Game', GreatAdventure.Game); // the main game logic
GreatAdventure.game.state.add("Level2", GreatAdventure.Level2);
GreatAdventure.game.state.add("Level3", GreatAdventure.Level3);


GreatAdventure.game.state.start('Boot');
