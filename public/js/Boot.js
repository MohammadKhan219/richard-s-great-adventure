var GreatAdventure = GreatAdventure || {};

GreatAdventure.Boot = function(){};

GreatAdventure.Boot.prototype = {
    preload: function() {
        this.load.image('logo', 'images/game assets/game_logo.png');
        this.game.load.audio("main_theme", "./images/game assets/Main_Menu.wav");
    },

    create: function() {
        this.game.stage.backgroundColor = '#000';
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.game.state.start('Preload');
    }
}