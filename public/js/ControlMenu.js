GreatAdventure.ControlMenu = function(){};

GreatAdventure.ControlMenu.prototype = {
    create: function() {
            this.control_menu_screen = this.add.sprite(0, 0, 'control_screen');

            this.back_button = this.add.sprite(this.game.world.centerX-125, 480, 'back_button');
            this.back_button.inputEnabled = true;
            this.back_button.events.onInputDown.add(function() {this.state.start('MainMenu')}, this);
        },

}